#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

double calculateCommission(int var_sales);

int main(int argc, const char* argv[])
{
    int var_sales;
    double var_commission;
    printf("Introduzca el total de las ventas del trimestre: \n");
    scanf("%d", &var_sales);
    var_commission = calculateCommission(var_sales);
    if (var_commission != -1)
    {
        printf("La comision del trimestre es %f\n", var_commission);
    }
    else
    {
        printf("Error, has ingresado un valor no valido\n");
    }
    return 0;
}

double calculateCommission(int var_sales)
{
    switch (var_sales)
    {
    case 0 ... 20000:
        return (var_sales * 0.05);
        break;
    case 20001 ... 50000:
        return ((var_sales * 0.07) + 1000);
        break;
    case 50001 ... INT_MAX:
        return ((var_sales * 0.1) + 3100);
    default:
        return -1;
        break;
    }
}