#include <stdio.h>

void multiplo(int n, int m);

int main()
{
    int n, m;
    printf("Introduzca un primer número: \n");
    scanf("%d", &n);
    printf("Introduzca un segundo número: \n");
    scanf("%d", &m);
    multiplo(n, m);
    return 0;
}

void multiplo(int n, int m)
{
    if (m % n == 0)
        printf("1\n\n");
    else
        printf("0\n\n");
}