#include <stdio.h>
#include <stdlib.h> //rand() y srand()
#include <time.h>   //time()

int generar_aleatorios(int num1, int num2);

int main (void) {
    int num, rand1, rand2, exit, next;
    exit = 0;
    srand(time(NULL));
    while (exit != 1)
    {
        printf("Para terminar el programa, escriba -1\n");
        rand1 = generar_aleatorios(1,10);
        rand2 = generar_aleatorios(1,10);
        next = 0;
        do
        {
            printf("¿Cuánto es %d veces %d?\n", rand1, rand2);
            scanf("%d", &num);
            if (num == rand1*rand2)
            {
                switch (generar_aleatorios(1,4))
                {
                case 1:
                    printf("Muy bien!\n");
                    break;
                case 2:
                    printf("Excelente!\n");
                    break;
                case 3:
                    printf("Buen trabajo!\n");
                    break;
                default:
                    printf("Sigue haciéndolo bien!\n");
                    break;
                }
                next = 1;
                break;
            } else if (num == -1)
            {
                exit = 1;
                break;
            } else
            {
                switch (generar_aleatorios(1,4))
                {
                case 1:
                    printf("No. Por favor trata de nuevo.\n");
                    break;
                case 2:
                    printf("Incorrecto. Trata una vez más.\n");
                    break;
                case 3:
                    printf("No te rindas!\n");
                    break;
                default:
                    printf("No. Trata de nuevo\n");
                    break;
                }
            }
        } while (exit != 1 || next != 1);
    }
    
    return 0;
}

int mayor(int num1, int num2) {
    if(num1 >= num2) {
        return num1;
    }
    else {
        return num2;
    }
}

int menor(int num1, int num2) {
    if(num1 <= num2) {
        return num1;
    }
    else {
        return num2;
    }
}

int generar_aleatorios(int num1, int num2) {
    int num, M, m;
    
    M = mayor(num1, num2);
    m = menor(num1, num2);    
    num = rand()%(M - m + 1) + m;
    return num;
}