#include <stdio.h>
#include <stdlib.h> //rand() y srand()
#include <time.h>   //time()


int mayor(int num1, int num2);
int menor(int num1, int num2);
int generar_aleatorios(int num1, int num2);
void imprimir_moneda(int lado);

int main (void) {
    srand(time(NULL)); //genera la semilla
    int lanzamientos, lado;
    int num_caras = 0, num_sellos = 0;
    printf("Digite el numero de lanzamientos: ");
    scanf("%d", &lanzamientos);
    for (int i = 0; i < lanzamientos; i++) {
        lado = generar_aleatorios(0,1);
        if (lado == 1) {
            num_sellos++;            
        }
        else {
            num_caras++;
        }
        imprimir_moneda(lado);
    }
    printf(", #de caras = %d, # de sellos = %d\n",num_caras,num_sellos);
    return 0;
}

int mayor(int num1, int num2) {
    if(num1 >= num2) {
        return num1;
    }
    else {
        return num2;
    }
}

int menor(int num1, int num2) {
    if(num1 <= num2) {
        return num1;
    }
    else {
        return num2;
    }
}

int generar_aleatorios(int num1, int num2) {
    int num, M, m;
    
    M = mayor(num1, num2);
    m = menor(num1, num2);    
    num = rand()%(M - m + 1) + m;
    return num;
}

void imprimir_moneda(int lado) {
    if(lado == 1) {
        printf("S");
    }
    else {
        printf("C");
    }
}
